(function($) {
  Drupal.behaviors.changeOpacity = {
    attach: function (context, settings) {
      document.querySelector('input[type="range"][name="FOO_img_opacity"]').onchange=changeEventHandler;
      function changeEventHandler(event) {
      var opacity = event.currentTarget.value;
      var fallbackOpacity = parseFloat(opacity) * 100;
      var imageElement = document.querySelector('img[id="FOO_imgurl-image-preview"]');
      imageElement.style.opacity = opacity;
      imageElement.style.filter = 'alpha(opacity='+fallbackOpacity+')'; // IE fallback
      };
    }
  };
})(jQuery);